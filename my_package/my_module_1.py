"""A module in my package.
"""

# @profile # Uncomment to analyze with line_profiler or memory_profiler
def my_function(x, y):
    """Add two numbers
    """
    return x + y
