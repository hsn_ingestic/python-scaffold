import pytest
from my_package.my_module_2 import my_function

def test_my_function():
    assert my_function(2, 3) == 6

def test_my_function_raises_exception():
    with pytest.raises(TypeError):
        my_function('a', 'b')
