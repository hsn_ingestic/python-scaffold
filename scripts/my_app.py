"""Example application
"""
from my_package.my_module_1 import my_function

def main():
    """Main function"""
    answer = my_function(29, 13)

    print('The answer is {}'.format(answer))

if __name__ == '__main__':
    main()
