# cProfile

cProfile measures the time spent on every function call. For a usage example, see the cprofile.sh script.

# Line profiler

For a fine-grained, line-by-line analysis, use line_profiler.

Install it with `conda install line_profiler`.

To profile a function, decorate its definition with `@profile`, call the function in a script, and pass the script to line_profiler (see example in line_profiler.sh).

# Memory profiler

The memory_profiler package analyses the memory usage line-by-line. Usage is similar to line_profiler.

Install it with `conda install memory_profiler`.

To profile a function, decorate its definition with `@profile`, call the function in a script, and pass the script to memory_profiler (see example in memory_profiler.sh).
