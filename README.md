# Python project scaffold

A Python project scaffold, set up with packaging (setuptools), tests (pytest), code analysis (Pylint), documentation generation (Sphinx), and profiling (cProfile, line_profiler, memory_profiler).

## Python setup

The easiest option is to install the [Anaconda](https://www.continuum.io/downloads) distribution, which includes Python plus many popular libraries. Prefer using Python 3.x.

## Project structure


```
scripts/                Application scripts
my_package/             Package supporting the application
tests/                  pytest unit tests
docs/                   Sphinx documentation
requirements/           conda or pip requirements
utils/profiling/        Tools for profiling the code
.pylintrc               Pylint code-analysis parameters
setup.py, ez_setup.py   Setuptools files for building the package
```

## Tests

For tests to run, the package must be importable. Install it in development mode with conda :
```sh
conda develop .      # install in development mode
conda develop -u .   # uninstall
```
or with pip :
```sh
pip install -e .          # install in editable mode, using setup.py
pip uninstall my_package  # uninstall
```

Run tests with `pytest -v`.


## Code analysis

Run Pylint :

```bash
pylint my_package
pylint scripts/my_app.py
```

## Documentation

Use [Sphinx](http://sphinx-doc.org/tutorial.html) for documenting your package.

### Set up

Create a docs/ folder and run `sphinx-quickstart` in it. Most default options are fine, except for autodoc: answer yes, preferably.

### Write documentation

Edit the .rst source files in the docs/ folder. To automatically include Python docstrings, use [autodoc directives](http://www.sphinx-doc.org/tutorial.html#autodoc).

Among possible documentation styles supported by Sphinx, the Google style yields legible docstrings. To use this style, activate the [napoleon extension](http://www.sphinx-doc.org/ext/napoleon.html): in docs/conf.py, add `'sphinx.ext.napoleon'` to the `extensions` list.

If you want to use the “Read the docs” Sphinx theme, install the Python package sphinx_rtd_theme.

### Build

Before building the documentation, adjust the version and release numbers in docs/conf.py (you may also choose to import the value defined in my_package/version.py).

To build the documentation, run `make html` in the docs/ folder (or ` sphinx-build ...`).

## Packaging

To build a new package version, update the version number in my_package/version.py and run `python setup.py sdist`.

## Profiling

See the README file in utils/profiling.

## Style

Follow [PEP 8](https://www.python.org/dev/peps/pep-0008/) and the [Google style guide](https://google.github.io/styleguide/pyguide.html).
