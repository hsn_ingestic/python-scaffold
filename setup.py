from setuptools import setup, find_packages

exec(open('my_package/version.py').read())

setup(
    name = "my_package",
    version = __version__,
    packages = find_packages(),

    install_requires = ['pandas>=0.14.0', 'pyodbc>=3.0.7'],

    author = "Camille Auneth",
    author_email = "camille.auneth@truc.com",
    description = "A one-line description of the package",
    keywords = ""
)
